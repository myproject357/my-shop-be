package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail,Integer>{
    
}
