package com.devcamp.shopplus.reposiroty;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.entity.Product;

public interface IProductRepository extends JpaRepository<Product,Integer>{
    @Query(value = "SELECT * FROM `products` WHERE `product_line_id` LIKE :product_line_id%", nativeQuery = true)
	List<Product> findProductByProductLine(@Param("product_line_id") Integer Id);
}
