package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);
    
}
