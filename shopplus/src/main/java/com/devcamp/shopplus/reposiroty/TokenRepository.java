package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Token;

public interface TokenRepository extends JpaRepository<Token, Long>{
    Token findByToken(String token);
    
}
