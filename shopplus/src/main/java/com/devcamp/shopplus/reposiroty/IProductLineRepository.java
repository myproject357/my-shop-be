package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine,Integer>{
    
}
