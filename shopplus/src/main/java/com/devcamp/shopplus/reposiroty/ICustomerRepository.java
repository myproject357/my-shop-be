package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer,Integer>{
    
}
