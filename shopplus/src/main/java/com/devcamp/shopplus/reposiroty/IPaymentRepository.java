package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment,Integer>{
    
}
