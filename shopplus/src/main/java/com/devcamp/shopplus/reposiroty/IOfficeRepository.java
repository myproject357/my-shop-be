package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Office;

public interface IOfficeRepository extends JpaRepository<Office,Integer>{
    
}
