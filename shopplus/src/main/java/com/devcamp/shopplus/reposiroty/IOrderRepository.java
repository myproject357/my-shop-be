package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Order;

public interface IOrderRepository extends JpaRepository<Order,Integer>{
    
}
