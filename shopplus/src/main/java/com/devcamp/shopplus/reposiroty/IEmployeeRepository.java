package com.devcamp.shopplus.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee,Integer>{
    
}
