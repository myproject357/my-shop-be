package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Employee;
import com.devcamp.shopplus.reposiroty.IEmployeeRepository;
import com.devcamp.shopplus.service.EmployeeService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class EmployeeController {
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    @Autowired
    EmployeeService pEmployeeService;

    // lay tat ca employee = service
    @GetMapping("/all-employees")
    public ResponseEntity<List<Employee>> getAllEmployee() {
        try {
            return new ResponseEntity<>(pEmployeeService.getAllEmployeeService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay employee theo id
    @GetMapping("/employee/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable("id") int id) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return new ResponseEntity<>(employeeData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao employee moi ko service
    @PostMapping("/employee/create")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee employee) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(employee.getId());
        try {
            if (employeeData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Employee already exsit  ");
            }
            Employee newEmployee = new Employee();
            newEmployee.setFirstName(employee.getFirstName());
            newEmployee.setLastName(employee.getLastName());
            newEmployee.setExtension(employee.getExtension());
            newEmployee.setEmail(employee.getEmail());
            newEmployee.setOfficeCode(employee.getOfficeCode());
            newEmployee.setReportTo(employee.getReportTo());
            newEmployee.setJobTitle(employee.getJobTitle());

            Employee saveRole = pEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }  

    // tao employee moi co service
    @PostMapping("/employee/create1")
    public ResponseEntity<Object> createEmployeeS(@Valid @RequestBody Employee employee) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(employee.getId());
        try {
            if (employeeData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Employee already exsit  ");
            }
            Employee newEmployee = new Employee();

            pEmployeeService.setgetEmployeeService(newEmployee, employee);

            Employee saveRole = pEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }
 
    // cap nhat employee ko co service
    @PutMapping("/employee/update/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @Valid @RequestBody Employee employee) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee newEmployee = new Employee();
            newEmployee.setFirstName(employee.getFirstName());
            newEmployee.setLastName(employee.getLastName());
            newEmployee.setExtension(employee.getExtension());
            newEmployee.setEmail(employee.getEmail());
            newEmployee.setOfficeCode(employee.getOfficeCode());
            newEmployee.setReportTo(employee.getReportTo());
            newEmployee.setJobTitle(employee.getJobTitle());

            Employee saveRole = pEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee: " + id);
        }
    }

    // cap nhat employee co service
    @PutMapping("/employee/update1/{id}")
    public ResponseEntity<Object> updateEmployeeS(@PathVariable("id") int id, @Valid @RequestBody Employee employee) {
        Optional<Employee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee newEmployee = new Employee();

            pEmployeeService.setgetEmployeeService(newEmployee, employee);

            Employee saveRole = pEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee: " + id);
        }
    }

    // xoa employee theo id
    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable("id") int id){
        try{
            pEmployeeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca employee (khuyen cao ko nen dung)
    @DeleteMapping("/employee/delete-all")
    public ResponseEntity<Object> deleteAllEmployee() {
        try{
            pEmployeeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
}
