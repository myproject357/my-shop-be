package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Office;
import com.devcamp.shopplus.reposiroty.IOfficeRepository;
import com.devcamp.shopplus.service.OfficeService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OfficeController {
    @Autowired
    IOfficeRepository pIOfficeRepository;

    @Autowired
    OfficeService pOfficeService;

    // lay tat ca office = service
    @GetMapping("/all-office")
    public ResponseEntity<List<Office>> getAllOfficees() {
        try {
            return new ResponseEntity<>(pOfficeService.getAllOfficeService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay office theo id
    @GetMapping("/office/{id}")
    public ResponseEntity<Object> getOfficeById(@PathVariable("id") int id) {
        Optional<Office> officeData = pIOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            return new ResponseEntity<>(officeData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao office moi ko service
    @PostMapping("/office/create")
    public ResponseEntity<Object> createOffice(@Valid @RequestBody Office office) {
        Optional<Office> officeData = pIOfficeRepository.findById(office.getId());
        try {
            if (officeData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Office already exsit  ");
            }
            Office newOffice = new Office();
            newOffice.setCity(office.getCity());
            newOffice.setCountry(office.getCountry());
            newOffice.setState(office.getState());
            newOffice.setAddressLine(office.getAddressLine());
            newOffice.setPhone(office.getPhone());
            newOffice.setTerritory(office.getTerritory());



            Office saveRole = pIOfficeRepository.save(office);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }

    // tao office moi co service
    @PostMapping("/office/create1")
    public ResponseEntity<Object> createOfficeS(@Valid @RequestBody Office office) {
        Optional<Office> officeData = pIOfficeRepository.findById(office.getId());
        try {
            if (officeData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Office already exsit  ");
            }
            Office newOffice = new Office();

            pOfficeService.setgetOfficeService(newOffice, office);

            Office saveRole = pIOfficeRepository.save(office);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }

    // cap nhat office ko co service
    @PutMapping("/office/update/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id, @Valid @RequestBody Office office) {
        Optional<Office> officeData = pIOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            Office newOffice = new Office();
            newOffice.setCity(office.getCity());
            newOffice.setCountry(office.getCountry());
            newOffice.setState(office.getState());
            newOffice.setAddressLine(office.getAddressLine());
            newOffice.setPhone(office.getPhone());
            newOffice.setTerritory(office.getTerritory());

            Office saveRole = pIOfficeRepository.save(office);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Office: " + id);
        }
    }

    // cap nhat office co service
    @PutMapping("/office/update1/{id}")
    public ResponseEntity<Object> updateOfficeS(@PathVariable("id") int id, @Valid @RequestBody Office office) {
        Optional<Office> officeData = pIOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            Office newOffice = new Office();

            pOfficeService.setgetOfficeService(newOffice, office);

            Office saveRole = pIOfficeRepository.save(office);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Office: " + id);
        }
    }

    // xoa office theo id
    @DeleteMapping("/office/delete/{id}")
    public ResponseEntity<Object> deleteOfficeById(@PathVariable("id") int id) {
        try {
            pIOfficeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca office (khuyen cao ko nen dung)
    @DeleteMapping("/office/delete-all")
    public ResponseEntity<Object> deleteAllOffice() {
        try {
            pIOfficeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
