package com.devcamp.shopplus.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Product;
import com.devcamp.shopplus.entity.ProductLine;
import com.devcamp.shopplus.reposiroty.IProductLineRepository;
import com.devcamp.shopplus.reposiroty.IProductRepository;
import com.devcamp.shopplus.service.ProductService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProductController {
    @Autowired 
    IProductRepository productRepository;

    @Autowired
    ProductService productService;

    @Autowired
    IProductLineRepository productLineRepository;

    // lay tat ca product = service
    @GetMapping("/all-product")
    public ResponseEntity<List<Product>> getAllProducts() {
        try {
            return new ResponseEntity<>(productService.getAllProductService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay product theo id
    @GetMapping("/product/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable("id") int id) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            return new ResponseEntity<>(productData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/productlike/{product_line_id}")
    public ResponseEntity<Object> getRealEstateComfirm(@PathVariable("product_line_id") Integer id) {
        try {
            ArrayList<Product> customers = new ArrayList<>();
            productRepository.findProductByProductLine(id).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tao product moi ko service
    @PostMapping("/product/create/{ProductLineId}")
    public ResponseEntity<Object> createProduct(@PathVariable("ProductLineId") int id ,@Valid @RequestBody Product product) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent()) {
			Product newProduct = new Product();
            newProduct.setProductCode(product.getProductCode());
            newProduct.setBuyPrice(product.getBuyPrice());
            newProduct.setProductDescripttion(product.getProductDescripttion());
            newProduct.setProductName(product.getProductName());
            newProduct.setProductScale(product.getProductScale());
            newProduct.setProductVendor(product.getProductScale());
            newProduct.setQuantityInStock(product.getQuantityInStock());
            newProduct.setOrderDetails(product.getOrderDetails());


			ProductLine _productLine = productLineData.get();
			newProduct.setProductLine(_productLine);

			Product saveOrder = productRepository.save(newProduct);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // tao product moi co service
    @PostMapping("/product/create1/{ProductLineId}")
    public ResponseEntity<Object> createProductS(@PathVariable("ProductLineId") int id ,@Valid @RequestBody Product product) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent()) {
			Product newProduct = new Product();

            productService.setgetProductService(newProduct, product);

			ProductLine _productLine = productLineData.get();
			newProduct.setProductLine(_productLine);

			Product saveOrder = productRepository.save(newProduct);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // cap nhat product ko co service
    @PutMapping("/product/update/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @Valid @RequestBody Product product) {
        Optional<Product> productData = productRepository.findById(id);
		if (productData.isPresent()) {
			Product newProduct = new Product();
            newProduct.setProductCode(product.getProductCode());
            newProduct.setBuyPrice(product.getBuyPrice());
            newProduct.setProductDescripttion(product.getProductDescripttion());
            newProduct.setProductName(product.getProductName());
            newProduct.setProductScale(product.getProductScale());
            newProduct.setProductVendor(product.getProductScale());
            newProduct.setQuantityInStock(product.getQuantityInStock());
            newProduct.setOrderDetails(product.getOrderDetails());

            Product saveRole = productRepository.save(product);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Product: " + id);
        }
    }

    // cap nhat product co service
    @PutMapping("/product/update1/{id}")
    public ResponseEntity<Object> updateProductS(@PathVariable("id") int id, @Valid @RequestBody Product product) {
        Optional<Product> productData = productRepository.findById(id);
		if (productData.isPresent()) {
			Product newProduct = new Product();

            productService.setgetProductService(newProduct, product);

            Product saveRole = productRepository.save(product);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Product: " + id);
        }
    }

    // xoa product theo id
    @DeleteMapping("/product/delete/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable("id") int id) {
        try {
            productRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca product (khuyen cao ko nen dung)
    @DeleteMapping("/product/delete-all")
    public ResponseEntity<Object> deleteAllProduct() {
        try {
            productRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }                
}
