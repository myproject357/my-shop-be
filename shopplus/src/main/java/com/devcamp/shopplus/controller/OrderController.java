package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Customer;
import com.devcamp.shopplus.entity.Order;
import com.devcamp.shopplus.reposiroty.ICustomerRepository;
import com.devcamp.shopplus.reposiroty.IOrderRepository;
import com.devcamp.shopplus.service.OrderService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderController {
    @Autowired
    IOrderRepository pOrderRepository;

    @Autowired
    OrderService pOrderService;

    @Autowired
    ICustomerRepository pCustomerRepository;

    // lay tat ca order = service
    @GetMapping("/all-order")
    public ResponseEntity<List<Order>> getAllOrders() {
        try {
            return new ResponseEntity<>(pOrderService.getAllOrderService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay order theo id
    @GetMapping("/order/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable("id") int id) {
        Optional<Order> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao order moi ko service
    @PostMapping("/order/create/{customerId}")
    public ResponseEntity<Object> createOrder(@PathVariable("customerId") int id ,@Valid @RequestBody Order order) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			Order newOrder = new Order();
			newOrder.setOrderDate(order.getOrderDate());
			newOrder.setComments(order.getComments());
			newOrder.setOrderDetails(order.getOrderDetails());
			newOrder.setRequiredDate(order.getRequiredDate());
			newOrder.setShippedDate(order.getShippedDate());
			newOrder.setStatus(order.getStatus());


			Customer _customer = customerData.get();
			newOrder.setCustomer(_customer);


			Order saveOrder = pOrderRepository.save(newOrder);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // tao order moi co service
    @PostMapping("/order/create1/{customerId}")
    public ResponseEntity<Object> createOrderS(@PathVariable("customerId") int id ,@Valid @RequestBody Order order) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			Order newOrder = new Order();

            pOrderService.setgetOrderService(newOrder, order);

			Customer _customer = customerData.get();
			newOrder.setCustomer(_customer);


			Order saveOrder = pOrderRepository.save(newOrder);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // cap nhat order ko co service
    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") int id, @Valid @RequestBody Order order) {
        Optional<Order> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
			Order newOrder = new Order();
			newOrder.setOrderDate(order.getOrderDate());
			newOrder.setComments(order.getComments());
			newOrder.setOrderDetails(order.getOrderDetails());
			newOrder.setRequiredDate(order.getRequiredDate());
			newOrder.setShippedDate(order.getShippedDate());
			newOrder.setStatus(order.getStatus());

            Order saveRole = pOrderRepository.save(order);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Order: " + id);
        }
    }

    // cap nhat order co service
    @PutMapping("/order/update1/{id}")
    public ResponseEntity<Object> updateOrderS(@PathVariable("id") int id, @Valid @RequestBody Order order) {
        Optional<Order> officeData = pOrderRepository.findById(id);
        if (officeData.isPresent()) {
			Order newOrder = new Order();

            pOrderService.setgetOrderService(newOrder, order);

            Order saveRole = pOrderRepository.save(order);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Order: " + id);
        }
    }

    // xoa order theo id
    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable("id") int id) {
        try {
            pOrderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca order (khuyen cao ko nen dung)
    @DeleteMapping("/order/delete-all")
    public ResponseEntity<Object> deleteAllOrder() {
        try {
            pOrderRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
