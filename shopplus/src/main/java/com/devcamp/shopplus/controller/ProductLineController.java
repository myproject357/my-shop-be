package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.ProductLine;
import com.devcamp.shopplus.reposiroty.IProductLineRepository;
import com.devcamp.shopplus.service.ProductLineService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProductLineController {
    @Autowired
    IProductLineRepository productLineRepository;

    @Autowired
    ProductLineService productLineService;

    // lay tat ca productline = service
    @GetMapping("/all-productline")
    public ResponseEntity<List<ProductLine>> getAllCustomer() {
        try {
            return new ResponseEntity<>(productLineService.getAllProductLineService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay productline theo id
    @GetMapping("/productline/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<ProductLine> customerData = productLineRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao productline moi ko service
    @PostMapping("/productline/create")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody ProductLine productLine) {
        Optional<ProductLine> productLineData = productLineRepository.findById(productLine.getId());
        try {
            if (productLineData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Productline already exsit  ");
            }
            ProductLine newProductLine = new ProductLine();
            newProductLine.setProductLine(productLine.getProductLine());
            newProductLine.setDescription(productLine.getDescription());


            ProductLine saveRole = productLineRepository.save(newProductLine);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    // tao productline moi co service
    @PostMapping("/productline/create1")
    public ResponseEntity<Object> createCustomerS(@Valid @RequestBody ProductLine productLine) {
        Optional<ProductLine> productLineData = productLineRepository.findById(productLine.getId());
        try {
            if (productLineData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Productline already exsit  ");
            }
            ProductLine newProductLine = new ProductLine();

            productLineService.setgetProductLineService(newProductLine, productLine);

            ProductLine saveRole = productLineRepository.save(newProductLine);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    // cap nhat productline ko co service
    @PutMapping("/productline/update/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @Valid @RequestBody ProductLine productLine) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            ProductLine newProductLine = new ProductLine();
            newProductLine.setProductLine(productLine.getProductLine());
            newProductLine.setDescription(productLine.getDescription());

            ProductLine saveRole = productLineRepository.save(productLine);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Productline: " + id);
        }
    }

    // cap nhat productline co service
    @PutMapping("/productline/update1/{id}")
    public ResponseEntity<Object> updateProductLineS(@PathVariable("id") int id, @Valid @RequestBody ProductLine productLine) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            ProductLine newProductLine = new ProductLine();

            productLineService.setgetProductLineService(newProductLine, productLine);

            ProductLine saveRole = productLineRepository.save(productLine);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Productline: " + id);
        }
    }

    // xoa productline theo id
    @DeleteMapping("/productline/delete/{id}")
    public ResponseEntity<Object> deleteProductLineById(@PathVariable("id") int id){
        try{
            productLineRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca productline (khuyen cao ko nen dung)
    @DeleteMapping("/productline/delete-all")
    public ResponseEntity<Object> deleteAllProductLine() {
        try{
            productLineRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }        
}
