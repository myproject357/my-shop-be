package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Customer;
import com.devcamp.shopplus.entity.Payment;
import com.devcamp.shopplus.reposiroty.ICustomerRepository;
import com.devcamp.shopplus.reposiroty.IPaymentRepository;
import com.devcamp.shopplus.service.PaymentService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class PaymentController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @Autowired 
    IPaymentRepository paymentRepository;

    @Autowired
    PaymentService paymentService;

    // lay tat ca payment = service
    @GetMapping("/all-payment")
    public ResponseEntity<List<Payment>> getAllPayments() {
        try {
            return new ResponseEntity<>(paymentService.getAllPaymentService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay payment theo id
    @GetMapping("/payment/{id}")
    public ResponseEntity<Object> getPaymentById(@PathVariable("id") int id) {
        Optional<Payment> orderData = paymentRepository.findById(id);
        if (orderData.isPresent()) {
            return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao payment moi ko service
    @PostMapping("/payment/create/{customerId}")
    public ResponseEntity<Object> createPayment(@PathVariable("customerId") int id ,@Valid @RequestBody Payment payment) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			Payment newPayment = new Payment();
			newPayment.setPaymentDate(payment.getPaymentDate());
			newPayment.setCheckNumber(payment.getCheckNumber());
			newPayment.setAmmount(payment.getAmmount());

			Customer _customer = customerData.get();
			newPayment.setCustomer(_customer);


			Payment saveOrder = paymentRepository.save(newPayment);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // tao payment moi co service
    @PostMapping("/payment/create1/{customerId}")
    public ResponseEntity<Object> createPaymentS(@PathVariable("customerId") int id ,@Valid @RequestBody Payment payment) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			Payment newPayment = new Payment();

            paymentService.setgetPaymentService(newPayment, payment);

			Customer _customer = customerData.get();
			newPayment.setCustomer(_customer);


			Payment saveOrder = paymentRepository.save(newPayment);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // cap nhat payment ko co service
    @PutMapping("/payment/update/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @Valid @RequestBody Payment payment) {
        Optional<Payment> paymentData = paymentRepository.findById(id);
        if (paymentData.isPresent()) {
			Payment newPayment = new Payment();
			newPayment.setPaymentDate(payment.getPaymentDate());
			newPayment.setCheckNumber(payment.getCheckNumber());
			newPayment.setAmmount(payment.getAmmount());

            Payment saveRole = paymentRepository.save(payment);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Payment: " + id);
        }
    }

    // cap nhat payment co service
    @PutMapping("/payment/update1/{id}")
    public ResponseEntity<Object> updatePaymentS(@PathVariable("id") int id, @Valid @RequestBody Payment payment) {
        Optional<Payment> paymentData = paymentRepository.findById(id);
        if (paymentData.isPresent()) {
			Payment newPayment = new Payment();

            paymentService.setgetPaymentService(newPayment, payment);

            Payment saveRole = paymentRepository.save(payment);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Payment: " + id);
        }
    }

    // xoa payment theo id
    @DeleteMapping("/payment/delete/{id}")
    public ResponseEntity<Object> deletePaymentById(@PathVariable("id") int id) {
        try {
            paymentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca payment (khuyen cao ko nen dung)
    @DeleteMapping("/payment/delete-all")
    public ResponseEntity<Object> deleteAllPayment() {
        try {
            paymentRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }            
}
