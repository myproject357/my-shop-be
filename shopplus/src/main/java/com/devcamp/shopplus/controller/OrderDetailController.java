package com.devcamp.shopplus.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Order;
import com.devcamp.shopplus.entity.OrderDetail;
import com.devcamp.shopplus.entity.Product;
import com.devcamp.shopplus.reposiroty.IOrderDetailRepository;
import com.devcamp.shopplus.reposiroty.IOrderRepository;
import com.devcamp.shopplus.reposiroty.IProductRepository;
import com.devcamp.shopplus.service.OrderDetailService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderDetailController {
    @Autowired
    IOrderDetailRepository pDetailRepository;

    @Autowired 
    OrderDetailService pDetailService;

    @Autowired
    IOrderRepository orderRepository;

    @Autowired
    IProductRepository productRepository;

    // lay tat ca orderdetail = service
    @GetMapping("/all-orderdetail")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
        try {
            return new ResponseEntity<>(pDetailService.getAllOrderDetailService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay orderdetail theo id
    @GetMapping("/orderdetail/{id}")
    public ResponseEntity<Object> getOrderDetailById(@PathVariable("id") int id) {
        Optional<OrderDetail> orderData = pDetailRepository.findById(id);
        if (orderData.isPresent()) {
            return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao orderdetail moi ko service
    @PostMapping("/orderdetail/create/{orderId}/{productId}")
    public ResponseEntity<Object> createOrderDetail(@PathVariable("orderId") int orderId,@PathVariable("productId") int productId ,@Valid @RequestBody OrderDetail orderDetail) {
        Optional<Order> orderData = orderRepository.findById(orderId);
        Optional<Product> productData = productRepository.findById(productId);
		if (orderData.isPresent() && productData.isPresent()) {
			OrderDetail newOrderDetail = new OrderDetail();
			newOrderDetail.setPriceEach(orderDetail.getPriceEach());
			newOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());

			Order _order = orderData.get();
			newOrderDetail.setOrder(_order);

            Product _product = productData.get();
			newOrderDetail.setProduct(_product);

			OrderDetail saveOrder = pDetailRepository.save(newOrderDetail);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // tao orderdetail moi co service
    @PostMapping("/orderdetail/create1/{customerId}/{productId}")
    public ResponseEntity<Object> createOrderDetailS(@PathVariable("orderId") int orderId,@PathVariable("productId") int productId ,@Valid @RequestBody OrderDetail orderDetail) {
        Optional<Order> orderData = orderRepository.findById(orderId);
        Optional<Product> productData = productRepository.findById(productId);
		if (orderData.isPresent() && productData.isPresent()) {
			OrderDetail newOrderDetail = new OrderDetail();

            pDetailService.setgetOrderDetailService(newOrderDetail, orderDetail);

			Order _order = orderData.get();
			newOrderDetail.setOrder(_order);

            Product _product = productData.get();
			newOrderDetail.setProduct(_product);

			OrderDetail saveOrder = pDetailRepository.save(newOrderDetail);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }

    // cap nhat orderdetail ko co service
    @PutMapping("/orderdetail/update/{id}")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable("id") int id, @Valid @RequestBody OrderDetail orderDetail) {
        Optional<OrderDetail> orderDetailData = pDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
			OrderDetail newOrderDetail = new OrderDetail();
			newOrderDetail.setPriceEach(orderDetail.getPriceEach());
			newOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());

            OrderDetail saveRole = pDetailRepository.save(orderDetail);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Orderdetail: " + id);
        }
    }

    // cap nhat orderdetail co service
    @PutMapping("/orderdetail/update1/{id}")
    public ResponseEntity<Object> updateOrderDetailS(@PathVariable("id") int id, @Valid @RequestBody OrderDetail orderDetail) {
        Optional<OrderDetail> orderDetailData = pDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
			OrderDetail newOrderDetail = new OrderDetail();

            pDetailService.setgetOrderDetailService(newOrderDetail, orderDetail);

            OrderDetail saveRole = pDetailRepository.save(orderDetail);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Orderdetail: " + id);
        }
    }

    // xoa orderdetail theo id
    @DeleteMapping("/orderdetail/delete/{id}")
    public ResponseEntity<Object> deleteOrderDetailById(@PathVariable("id") int id) {
        try {
            pDetailRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca orderdetail (khuyen cao ko nen dung)
    @DeleteMapping("/orderdetail/delete-all")
    public ResponseEntity<Object> deleteAllOrderDetail() {
        try {
            pDetailRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }            
}
