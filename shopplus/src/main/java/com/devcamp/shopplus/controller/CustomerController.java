package com.devcamp.shopplus.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.entity.Customer;
import com.devcamp.shopplus.reposiroty.ICustomerRepository;
import com.devcamp.shopplus.service.CustomerSerivce;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @Autowired
    CustomerSerivce pCustomerSerivce;

    // lay tat ca customer = service
    @GetMapping("/all-customers")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            return new ResponseEntity<>(pCustomerSerivce.getAllCustomerService(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lay customer theo id
    @GetMapping("/customer/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tao customer moi ko service
    @PostMapping("/customer/create")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer customer) {
        Optional<Customer> customerData = pCustomerRepository.findById(customer.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            Customer newCustomer = new Customer();
            newCustomer.setFirstName(customer.getFirstName());
            newCustomer.setLastName(customer.getLastName());
            newCustomer.setAddress(customer.getAddress());
            newCustomer.setCountry(customer.getCountry());
            newCustomer.setCity(customer.getCity());
            newCustomer.setPostalCode(customer.getPostalCode());
            newCustomer.setPhoneNumber(customer.getPhoneNumber());
            newCustomer.setState(customer.getState());
            newCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
            newCustomer.setCreditLimit(customer.getCreditLimit());
            newCustomer.setOrders(customer.getOrders());
            newCustomer.setPayments(customer.getPayments());

            Customer saveRole = pCustomerRepository.save(newCustomer);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    // tao customer moi co service
    @PostMapping("/customer/create1")
    public ResponseEntity<Object> createCustomerS(@Valid @RequestBody Customer customer) {
        Optional<Customer> customerData = pCustomerRepository.findById(customer.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            Customer newCustomer = new Customer();

            pCustomerSerivce.setgetCustomerService(newCustomer, customer);

            Customer saveRole = pCustomerRepository.save(newCustomer);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    // cap nhat customer ko co service
    @PutMapping("/customer/update/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @Valid @RequestBody Customer customer) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            Customer newCustomer = new Customer();
            newCustomer.setFirstName(customer.getFirstName());
            newCustomer.setLastName(customer.getLastName());
            newCustomer.setAddress(customer.getAddress());
            newCustomer.setCountry(customer.getCountry());
            newCustomer.setCity(customer.getCity());
            newCustomer.setPostalCode(customer.getPostalCode());
            newCustomer.setPhoneNumber(customer.getPhoneNumber());
            newCustomer.setState(customer.getState());
            newCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
            newCustomer.setCreditLimit(customer.getCreditLimit());
            newCustomer.setOrders(customer.getOrders());
            newCustomer.setPayments(customer.getPayments());
            Customer saveRole = pCustomerRepository.save(customer);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    // cap nhat customer co service
    @PutMapping("/customer/update1/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Valid @RequestBody Customer customer) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            Customer newCustomer = new Customer();

            pCustomerSerivce.setgetCustomerService(newCustomer, customer);

            Customer saveRole = pCustomerRepository.save(customer);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    // xoa customer theo id
    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCustomerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xoa tat ca customer (khuyen cao ko nen dung)
    @DeleteMapping("/customer/delete-all")
    public ResponseEntity<Object> deleteAllCustomer() {
        try{
            pCustomerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
