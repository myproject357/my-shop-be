package com.devcamp.shopplus.service;

import com.devcamp.shopplus.entity.User;
import com.devcamp.shopplus.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
