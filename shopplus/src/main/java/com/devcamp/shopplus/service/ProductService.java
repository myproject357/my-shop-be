package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Product;
import com.devcamp.shopplus.reposiroty.IProductRepository;

@Service
public class ProductService {
    @Autowired
    IProductRepository pProductRepository;
    public ArrayList<Product> getAllProductService() {
        ArrayList<Product> products = new ArrayList<Product>();
        pProductRepository.findAll().forEach(products::add);
        return products;
    }
public void setgetProductService(Product newProduct, Product product) {
    newProduct.setProductCode(product.getProductCode());
    newProduct.setBuyPrice(product.getBuyPrice());
    newProduct.setProductDescripttion(product.getProductDescripttion());
    newProduct.setProductName(product.getProductName());
    newProduct.setProductScale(product.getProductScale());
    newProduct.setProductVendor(product.getProductScale());
    newProduct.setQuantityInStock(product.getQuantityInStock());
    newProduct.setOrderDetails(product.getOrderDetails());
} 
}
