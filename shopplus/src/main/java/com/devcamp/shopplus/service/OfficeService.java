package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Office;
import com.devcamp.shopplus.reposiroty.IOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    IOfficeRepository pIOfficeRepository;
    public ArrayList<Office> getAllOfficeService() {
        ArrayList<Office> offices = new ArrayList<Office>();
        pIOfficeRepository.findAll().forEach(offices::add);
        return offices;
    }

    public void setgetOfficeService(Office newOffice , Office office) {
        newOffice.setCity(office.getCity());
        newOffice.setCountry(office.getCountry());
        newOffice.setState(office.getState());
        newOffice.setAddressLine(office.getAddressLine());
        newOffice.setPhone(office.getPhone());
        newOffice.setTerritory(office.getTerritory());

    }
}
