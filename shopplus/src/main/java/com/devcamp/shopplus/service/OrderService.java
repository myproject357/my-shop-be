package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Order;
import com.devcamp.shopplus.reposiroty.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository pOrderRepository;
    public ArrayList<Order> getAllOrderService() {
        ArrayList<Order> orders = new ArrayList<Order>();
        pOrderRepository.findAll().forEach(orders::add);
        return orders;
    }

    public void setgetOrderService(Order newOrder , Order order) {
        newOrder.setOrderDate(order.getOrderDate());
        newOrder.setComments(order.getComments());
        newOrder.setOrderDetails(order.getOrderDetails());
        newOrder.setRequiredDate(order.getRequiredDate());
        newOrder.setShippedDate(order.getShippedDate());
        newOrder.setStatus(order.getStatus());
    }
}
