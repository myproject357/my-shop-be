package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Payment;
import com.devcamp.shopplus.reposiroty.IPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    IPaymentRepository paymentRepository;
    public ArrayList<Payment> getAllPaymentService() {
        ArrayList<Payment> payments = new ArrayList<Payment>();
        paymentRepository.findAll().forEach(payments::add);
        return payments;
    }

    public void setgetPaymentService(Payment newPayment , Payment payment) {
        newPayment.setPaymentDate(payment.getPaymentDate());
        newPayment.setCheckNumber(payment.getCheckNumber());
        newPayment.setAmmount(payment.getAmmount());
    }
}
