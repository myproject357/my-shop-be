package com.devcamp.shopplus.service;

import com.devcamp.shopplus.entity.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);
}
