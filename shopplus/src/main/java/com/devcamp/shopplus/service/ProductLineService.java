package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.ProductLine;
import com.devcamp.shopplus.reposiroty.IProductLineRepository;

@Service
public class ProductLineService {
    @Autowired
    IProductLineRepository pProductLineRepository;
    public ArrayList<ProductLine> getAllProductLineService() {
        ArrayList<ProductLine> productLines = new ArrayList<ProductLine>();
        pProductLineRepository.findAll().forEach(productLines::add);
        return productLines;
    }
    public void setgetProductLineService(ProductLine newProductLine , ProductLine productLine) {
        newProductLine.setProductLine(productLine.getProductLine());
        newProductLine.setDescription(productLine.getDescription());
    }
}
