package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Employee;
import com.devcamp.shopplus.reposiroty.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired 
    IEmployeeRepository pEmployeeRepository;
    public ArrayList<Employee> getAllEmployeeService() {
        ArrayList<Employee> employees = new ArrayList<Employee>();
        pEmployeeRepository.findAll().forEach(employees::add);
        return employees;
    }

    public void setgetEmployeeService(Employee newEmployee , Employee employee) {
        newEmployee.setFirstName(employee.getFirstName());
        newEmployee.setLastName(employee.getLastName());
        newEmployee.setExtension(employee.getExtension());
        newEmployee.setEmail(employee.getEmail());
        newEmployee.setOfficeCode(employee.getOfficeCode());
        newEmployee.setReportTo(employee.getReportTo());
        newEmployee.setJobTitle(employee.getJobTitle());

    }
}
