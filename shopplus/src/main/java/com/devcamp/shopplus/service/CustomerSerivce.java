package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.Customer;
import com.devcamp.shopplus.reposiroty.ICustomerRepository;

@Service
public class CustomerSerivce {
    @Autowired
    ICustomerRepository pCustomerRepository;
    public ArrayList<Customer> getAllCustomerService() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        pCustomerRepository.findAll().forEach(customers::add);
        return customers;
    }

    public void setgetCustomerService(Customer newCustomer , Customer customer) {
        newCustomer.setFirstName(customer.getFirstName());
        newCustomer.setLastName(customer.getLastName());
        newCustomer.setAddress(customer.getAddress());
        newCustomer.setCountry(customer.getCountry());
        newCustomer.setCity(customer.getCity());
        newCustomer.setPostalCode(customer.getPostalCode());
        newCustomer.setPhoneNumber(customer.getPhoneNumber());
        newCustomer.setState(customer.getState());
        newCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
        newCustomer.setCreditLimit(customer.getCreditLimit());
        newCustomer.setOrders(customer.getOrders());
        newCustomer.setPayments(customer.getPayments());
    }
}
