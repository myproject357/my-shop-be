package com.devcamp.shopplus.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.entity.OrderDetail;
import com.devcamp.shopplus.reposiroty.IOrderDetailRepository;

@Service
public class OrderDetailService {
    @Autowired 
    IOrderDetailRepository pDetailRepository;
    public ArrayList<OrderDetail> getAllOrderDetailService() {
        ArrayList<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
        pDetailRepository.findAll().forEach(orderDetails::add);
        return orderDetails;
    }

    public void setgetOrderDetailService(OrderDetail newOrderDetail , OrderDetail orderDetail) {
        newOrderDetail.setPriceEach(orderDetail.getPriceEach());
        newOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
    }
}
